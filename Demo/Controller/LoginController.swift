//
//  LoginController.swift
//  Demo
//
//  Created by ND on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import UIKit

//protocol LoginViewData {
//  
//  var username : String { get }
//  var password : String { get }
//  
//}

class LoginController: UIViewController {
  
  
  @IBOutlet weak private var txtEmail: UITextField!
  @IBOutlet weak private var txtPassword: UITextField!
  
  
  override func viewDidLoad() {
        
        /// doing the stuff after view is loaded
        self.title = "Login"
    }
    
  override func didReceiveMemoryWarning() {
      
  }
  
  @IBAction func doLogin(sender: AnyObject) {
    
    let loginViewModel = LoginViewModel(username: txtEmail.text, password: txtPassword.text,fullname:nil) // create viewmodel for login to interact with model
    
    if let loginMsg = loginViewModel.isParamSet {
      
      // show error on empty or invalid field
      UIAlertController.presentAlertWithTitle("Warning", message: loginMsg, inController: self)
    }
    else {

        // check authorization
        loginViewModel.checkAuthorization()
        
        // subscribe for authorization check, don't worry about unsubscribe it will automatically unsubscribe on removing the controller
        
        loginViewModel.isAuthorized.subscribe(self) {
            [unowned self] newValue in
            
            if newValue! as Bool {
               self.showDashboard()
            }
            else {
                
                 UIAlertController.presentAlertWithTitle("Warning", message: "Please enter correct email and password.", inController: self)
            }
            
        }
    }
    
  }
    
  func showDashboard() {
    
    if let nav = self.navigationController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewControllerWithIdentifier("DashboardStory") as! DashboardController
        
        let navController = UINavigationController.init(rootViewController: vc)
        
        //vc.userID = shareID
        
        nav.presentViewController(navController, animated: true, completion: nil)
    }
    
  }
    
}



