//
//  SignupController.swift
//  Demo
//
//  Created by Nikunj Damani on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import UIKit

class SignupController: UIViewController {
  
  @IBOutlet weak private var txtFullname: UITextField!
  
  @IBOutlet weak private var txtEmail: UITextField!
  
  @IBOutlet weak private var txtPassword: UITextField!
  
  override func viewDidLoad() {
    
    self.title = "Sign up"
    
  }
  
  override func didReceiveMemoryWarning() {
    
  }
  
  
  @IBAction func createAccount(sender: AnyObject) {
    
    var signupViewModel = SignupViewModel() // create viewmodel for login to interact with model
    signupViewModel.username = txtEmail.text // assign value from text field
    signupViewModel.password = txtPassword.text // assign value from text field
    signupViewModel.fullname = txtFullname.text // assign value from text field
    
    
    if let signupMsg = signupViewModel.isParamSet {
      
      // show error on empty or invalid field
      UIAlertController.presentAlertWithTitle("Warning", message: signupMsg, inController: self)
        
    }
    else {
        
      signupViewModel.createUser()
      
      signupViewModel.isAccCreated.subscribe(self) {
        [unowned self] newValue in
        
        if newValue! as Bool {
          signupViewModel.createAccount()
        }
        else {
          
          UIAlertController.presentAlertWithTitle("Error", message: "We are not able to prcoess your request right now, please try after sometime.", inController: self)
        }
        
      } //observer for user creation
      
      signupViewModel.isAuthorized.subscribe(self) {
        [unowned self] newValue in
        
        if newValue! as Bool {
          self.showDashboard()
        }
        else {
          
          UIAlertController.presentAlertWithTitle("Error", message: "We are not able to prcoess your request right now, please try after sometime.", inController: self)
        }
        
      } //observer for user authorization
      
    }
  }
  
  @IBAction func cancelSignup(sender: AnyObject) {
    
    // pop view controller from navigation stack
    self.navigationController?.popViewControllerAnimated(true)
    
  }
    
    
  func showDashboard() {
        
        if let nav = self.navigationController
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let vc = storyboard.instantiateViewControllerWithIdentifier("DashboardStory") as! DashboardController
            
            let navController = UINavigationController.init(rootViewController: vc)
            
            //vc.userID = shareID
            
            nav.presentViewController(navController, animated: true, completion: nil)
        }
        
    }


}
