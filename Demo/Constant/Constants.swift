//
//  Constants.swift
//  Demo
//
//  Created by ND on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation

struct URLFRB {
    
    /// base URL for firebase
    static let BASE = "https://quoteposting.firebaseio.com"
    
    /// url for the users
    static let USERS = "\(URLFRB.BASE)/users"
    
    /// url for the quotes
    static let QUOTES = "\(URLFRB.BASE)/quotes"
    
}


