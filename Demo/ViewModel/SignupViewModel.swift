//
//  SignupViewModel.swift
//  Demo
//
//  Created by Nikunj Damani on 28/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation


struct SignupViewModel : AuthorizeUser {
  
  var username : String?
  var password: String?
  var fullname: String?
  
  var isParamSet : String? {
    
    guard fullname!.hasLength   else { return "Please fill in the fullname field." } // check for the non empty fullname
    guard username!.hasLength   else { return "Please fill in the email field." } // check for the non empty email
    guard username!.isValidEmail else { return "Please enter valid email." } // check for the  valid email
    guard password!.hasLength   else { return "Please fill in the password field." } // check for the non empty password
    
    return nil;
    
  }
  
  var isAuthorized = Property<Bool?>(nil) // value is observable
  
  var isAccCreated = Property<Bool?>(nil) // value is observable
  
  
  func createUser() {
    UserModel.userModel.createUser(self)
  }
  
  func createAccount() {
    UserModel.userModel.createAccount(self)
  }

}






