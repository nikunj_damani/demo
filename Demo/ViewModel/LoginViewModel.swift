//
//  LiginViewModel.swift
//  Demo
//
//  Created by Nikunj Damani on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation
import Firebase

protocol ProfileDataSource {
  
  var username : String? { get }
  var password : String? { get }
  var fullname : String? { get }
  
}

protocol AuthorizeUser : ProfileDataSource {
  
  var isParamSet : String? { get }
  var isAuthorized : Property<Bool?> { get }
  var isAccCreated : Property<Bool?>{ get }
  func checkAuthorization()
  
}


/// default init of protocol
extension AuthorizeUser {
  
    var isParamSet : String? {
        
        guard username!.hasLength   else { return "Please fill in the email field." } // check for the non empty email
        guard username!.isValidEmail else { return "Please enter valid email." } // check for the  valid email
        guard password!.hasLength   else { return "Please fill in the password field." } // check for the non empty password
        
        return nil;
        
    }
    
    func checkAuthorization() {
        UserModel.userModel.tryLogin(self)
    }
  
  
  
}

/// ViewModel which will interct between controller and model
struct LoginViewModel : AuthorizeUser {
    
    var username: String?
    var password: String?
    var fullname: String? 
  
    let isAuthorized = Property<Bool?>(nil) // value is observable
  
    let isAccCreated = Property<Bool?>(nil) // value is observable
    // override function if you want
    
}



