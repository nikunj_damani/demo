//
//  UserDefaultsHelper.swift
//  Demo
//
//  Created by Nikunj Damani on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation

public extension NSUserDefaults {
  
  
  /// Get the Bool value from user defaults
  ///- returns: A Bool value stored in user defaults for a key
  public class func getBoolValueForConfigurationKey(objectKey : String) -> Bool {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.synchronize()
    return defaults.boolForKey(objectKey)
  }
  
  /// Get the String value from user defaults
  ///- returns: A String value stored in user defaults for a key
  public class func getStringValueForConfigurationKey(objectKey : String) -> String? {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.synchronize()
    
    if defaults.stringForKey(objectKey) == nil
    {
      return nil
    }
    else
    {
      return defaults.stringForKey(objectKey)
    }
    
  }
  
  /// Set the Bool value in user defaults for a key
  public class func setBoolValueForConfigurationKey(objectKey : String, boolValue : Bool) {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.synchronize()
    
    defaults.setBool(boolValue, forKey: objectKey)
    
    defaults.synchronize()
  }
  
  /// Set the String value in user defaults for a key
  public class func setStringValueForConfigurationKey(objectKey : String, value : String) {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    defaults.synchronize()
    
    defaults.setObject(value, forKey: objectKey)
    
    defaults.synchronize()
    
  }
  
}


