//
//  StringHelper.swift
//  Demo
//
//  Created by Nikunj Damani on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation

/// check the string contains valid email or not
extension String {
 
  var isValidEmail: Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
    let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(self)
  }
  
    var hasLength: Bool {
        
        if characters.count > 0 {
            return true
        }
        else {
            return false
        }
    }
  
}