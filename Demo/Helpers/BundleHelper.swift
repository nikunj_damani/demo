//
//  BundleHelper.swift
//  Demo
//
//  Created by ND on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation


///Helper for `print` function with a filter for use in debug mode only
public func P(string: String) {
    #if RELEASE
    #else
        print(string)
    #endif
}

///Get the current version of the app
///- returns: A string representing the version of the app {CFBundleShortVersionString}.{CFBundleVersion}
public func appVersion() -> String {
    let mainBundle = NSBundle.mainBundle()
    if let bundleDictionary = mainBundle.infoDictionary {
        if let shortVersion = bundleDictionary["CFBundleShortVersionString"] as? String {
            if let bundleVersion = bundleDictionary["CFBundleVersion"] as? String {
                return NSString(format: "%@.%@", shortVersion, bundleVersion) as String
            }
        }
    }
    
    return ""
}

