//
//  UserModel.swift
//  Demo
//
//  Created by ND on 27/06/16.
//  Copyright © 2016 Veepal. All rights reserved.
//

import Foundation
import Firebase


class UserModel {
    
    
    static let userModel = UserModel()
  
  

//  var CURRENT_USER_REF: Firebase { /// get user reference if it's already sign up/in
//    
//    let userID = NSUserDefaults.getStringValueForConfigurationKey("uid")
//    
//    let currentUser = Firebase(url: "\(URLFRB.USERS)").childByAppendingPath(userID)
//    
//    return currentUser!
//  }
//  
//  
//  func createNewAccount(uid: String, user: Dictionary<String, String>) { /// create user new user on firebase
//    _USER_REF.childByAppendingPath(uid).setValue(user)
//  }

    /// try to login using username and password
    
    func tryLogin(authorization : AuthorizeUser) {
    
    FIRAuth.auth()?.signInWithEmail(authorization.username!, password: authorization.password!) { (user, error) in
    
        if error != nil {
            authorization.isAuthorized.value = false
        } else {
             authorization.isAuthorized.value = true
        }
        
    }

  }
  
  func createUser(authorization:AuthorizeUser) {
        
        FIRAuth.auth()?.createUserWithEmail(authorization.username!, password: authorization.password!) { (user, error) in
            
            if error != nil {
              authorization.isAccCreated.value = false
            } else {
              authorization.isAccCreated.value = true
            }

        }
    }
  
  func createAccount(authorization : AuthorizeUser) {
    
    let user = FIRAuth.auth()?.currentUser
    if let user = user {
      let changeRequest = user.profileChangeRequest()
      
      changeRequest.displayName = authorization.fullname
     
      changeRequest.commitChangesWithCompletion { error in
        if let error = error {
          print(error)
          authorization.isAuthorized.value = false
        } else {
          authorization.isAuthorized.value = true
        }
      }
    }
    else{
      authorization.isAuthorized.value = false
    }
    
  } // createAccount()

}